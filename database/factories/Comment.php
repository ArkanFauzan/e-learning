<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'subject' => $faker->sentence(7),
        'user_id' => User::all()->random()->first(),
    ];
});

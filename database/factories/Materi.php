<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Materi;
use Faker\Generator as Faker;
use App\Course;

$factory->define(Materi::class, function (Faker $faker) {
    return [
        'course_id'=> Course::all()->random()->id,
        'title'=>$faker->sentence(7),
        'link_youtube' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/6OTBeW-SIh8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        'description' => $faker->sentence(40)
    ];
});

@extends('layouts.app')

@section('content')
<div class="container">
    <course-component />
</div>

<div class="container py-3">
    <comment-component />
</div>
@endsection
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('course-component', require('./components/course/CourseComponent.vue').default);
Vue.component('course-create-component', require('./components/course/CourseCreateComponent.vue').default);
Vue.component('course-edit-component', require('./components/course/CourseEditComponent.vue').default);
Vue.component('detail-course-component', require('./components/course/DetailCourseComponent.vue').default);

Vue.component('materi-component', require('./components/course/MateriComponent.vue').default);
Vue.component('materi-create-component', require('./components/course/MateriCreateComponent.vue').default);
Vue.component('materi-edit-component', require('./components/course/MateriEditComponent.vue').default);

Vue.component('comment-component', require('./components/comment/CommentComponent.vue').default);
Vue.component('comment-message-component', require('./components/comment/CommentMessageComponent.vue').default);
Vue.component('comment-form-component', require('./components/comment/CommentFormComponent.vue').default);

Vue.component('donate-component', require('./components/midtrans/DonateComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

import BusEvent from './bus'

Echo.join('new-comment')
    .listen('NewComment', (e) => {
        BusEvent.$emit('comment.sent', e.data);
    });
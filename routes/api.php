<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/course', 'CourseController@all');
// Route::post('/course', 'CourseController@create');
// Route::patch('/course/{id}', 'CourseController@update');

// Route::get('/materi/{id}', 'MateriController@show');
// Route::post('/materi', 'MateriController@create');
// Route::patch('/materi/{id}', 'MateriController@update');

// GET /course
// GET /course/{id}
// PUT /course/{id}
// POST /course
// DELETE /course/{id}

// midtrans
Route::post('/generate','MidtransController@generate');
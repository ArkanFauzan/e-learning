<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/course');
});

Auth::routes();


Route::middleware('auth')->group(function () {
    
    Route::view('/course', 'course');
    Route::view('/materi/detail/{id}', 'detail_materi');
    Route::view('/donate','midtrans.donate');
    
    Route::middleware('admin')->group(function () {
        Route::view('/course/create', 'course_create');
        Route::view('/course/edit/{id}', 'course_edit');
        Route::view('/materi/create', 'materi_create');
        Route::view('/materi/edit/{id}', 'materi_edit');

        // Logs
        Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Route api ke database
    Route::prefix('api')->group(function () {
        Route::get('/course', 'CourseController@all');
        Route::get('/course/{id}', 'CourseController@show');
        Route::get('/materi/{id}', 'MateriController@show');

        
        Route::middleware('admin')->group(function () {
            Route::post('/course', 'CourseController@create');
            Route::patch('/course/{id}', 'CourseController@update');
            Route::delete('/course/{id}', 'CourseController@delete');
            
            Route::post('/materi', 'MateriController@create');
            Route::patch('/materi/{id}', 'MateriController@update');
            Route::delete('/materi/{id}', 'MateriController@delete');
        });
        
        // route comment
        Route::get('/comment','CommentController@all');
        Route::post('/comment','CommentController@create');
    });
});

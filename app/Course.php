<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\support\Str;

class Course extends Model
{
    protected $guarded = [];

    // uuid
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (!$model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'id';
    }

    public function materi()
    {
        return $this->hasMany(Materi::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Resources\CommentResource;
use App\Events\NewComment;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{
    public function all()
    {
        $comments = Comment::orderBy('created_at', 'desc')->take(10)->get();

        return CommentResource::collection($comments);
    }

    public function create(Request $request)
    {
        $comment = Comment::create([
            'subject' => $request->subject,
            'user_id' => auth()->user()->id,
        ]);

        broadcast(new NewComment($comment))->toOthers();

        Log::info('Create new comment by: '.auth()->user()->email);
        return new CommentResource($comment);
    }
}

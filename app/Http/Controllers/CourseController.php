<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Http\Resources\CourseResource;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{
    public function all()
    {
        $course = Course::orderBy('created_at', 'asc')->get();

        return CourseResource::collection($course);
    }

    public function show(Course $id)
    {
        return new CourseResource($id);
    }

    public function create(Request $request)
    {
        $request->validate([
            'course' => ['required', 'unique:courses,course'],
            'description' => ['required', 'min:10']
        ]);

        $course = Course::create([
            'course' => $request->course,
            'description' => $request->description,
        ]);
        
        Log::info('Create course, id: '.$course->id.', by: '.auth()->user()->email);
        return new CourseResource($course);
    }

    public function update(Request $request, Course $id)
    {
        $request->validate([
            'course' => ['required', 'unique:courses,course'],
            'description' => ['required', 'min:10']
        ]);

        $id->update([
            'course' => $request->course,
            'description' => $request->description,
        ]);
        
        Log::info('Update course, id: '.$id->id.', by: '.auth()->user()->email);
        return new CourseResource($id);
    }

    public function delete(Course $id)
    {
        $course = $id;
        $id->delete();

        Log::info('Delete Course, id: '.$course->id.', by: '.auth()->user()->email);
    }
}

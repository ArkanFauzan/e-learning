<?php

namespace App\Http\Controllers;

use App\Http\Resources\MateriResource;
use Illuminate\Http\Request;
use App\Materi;
use Illuminate\Support\Facades\Log;

class MateriController extends Controller
{
    public function show(Materi $id){
        
        return new MateriResource($id);
    }

    public function create(Request $request)
    {
        $request->validate([
            'course_id'=> ['required'],
            'title'=> ['required','min:5'],
            'link_youtube'=>['required'],
            'description'=>['required','min:10'],
        ]);

        $materi = Materi::create([
            'course_id' => $request->course_id,
            'title' => $request->title,
            'link_youtube' => $request->link_youtube,
            'description' => $request->description,
        ]);

        Log::info('Create materi, id: '.$materi->id.', by: '.auth()->user()->email);
        return new MateriResource($materi);
    }

    public function update(Request $request, $id)
    {
        $this -> validate($request,[
            'course_id' => 'required',
            'title' => 'required',
            'link_youtube' => 'required',
            'description' => 'required'
        ]);
        $materi = Materi::findOrFail($id);
        $materi ->update([
            'course_id' => $request->course_id,
            'title' => $request->title,
            'link_youtube' => $request->link_youtube,
            'description' => $request->description,
        ]);

        Log::info('Update materi, id: '.$materi->id.', by: '.auth()->user()->email);
        return new MateriResource($materi);
    }

    public function delete(Materi $id){
        $materi = $id;
        $id->delete();
        Log::info('Delete materi, id: '.$materi->id.', by: '.auth()->user()->email);
    }
}

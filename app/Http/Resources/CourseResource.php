<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Materi;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $materi= Materi::where('course_id',$this->id)->orderBy('created_at','asc')->get();
        return [
            'id' => $this->id,
            'course' => $this->course,
            'description' => $this->description,
            'materi' => $materi
        ];
    }
}

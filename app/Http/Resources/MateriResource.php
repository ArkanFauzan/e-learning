<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MateriResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'course_id'=>$this->course->id,
            'id'=>$this->id,
            'title'=>$this->title,
            'link_youtube'=>$this->link_youtube,
            'description'=>$this->description
        ];
    }
}
